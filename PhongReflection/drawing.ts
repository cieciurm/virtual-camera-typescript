﻿module Drawing {
    export class Drawer {
        canvas: HTMLCanvasElement;
        circleRadius: number;

        constructor(canvas: HTMLCanvasElement) {
            this.canvas = canvas;
        }

        drawCircle(radius: number, color: string): void {
            this.circleRadius = radius;

            var context = this.canvas.getContext("2d");
            var centerX = this.canvas.width / 2;
            var centerY = this.canvas.height / 2;

            context.beginPath();
            context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
            context.fillStyle = color;
            context.fill();
            //context.lineWidth = 5;
            //context.strokeStyle = '#003300';
            //context.stroke();
        }

        isBackground(x: number, y: number): boolean {
            var context = this.canvas.getContext("2d");
            var imageData = context.getImageData(x, y, 1, 1).data;

            //console.log(imageData);

            if (imageData[0] == 0 && imageData[1] == 0 && imageData[2] == 0 && imageData[3] == 255)
                return true;

            return false;
        }

        setPixelColor(x: number, y: number, color: number[]) {
            var context = this.canvas.getContext("2d");

            var imageData = context.getImageData(x, y, 1, 1);

            var grayscale = (color[0] + color[1] + color[2]) / 3;

            //imageData.data[0] = color[0];
            //imageData.data[1] = color[1];
            //imageData.data[2] = color[2];

            imageData.data[0] = grayscale;
            imageData.data[1] = grayscale;
            imageData.data[2] = grayscale;

            context.putImageData(imageData, x, y);
        }

        calcZ(x, y) {
            var radius = this.circleRadius;
            var z = Math.sqrt(radius * radius - x * x - y * y);
            return Math.round(z);
        }
    }
} 