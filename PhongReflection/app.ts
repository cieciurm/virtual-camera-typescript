﻿import Drawer = Drawing.Drawer;
import Material = Phong.Material;

window.onload = () => {
    var step = 20;
    var canvas1: any = document.getElementById("canvas1");
    var canvas2: any = document.getElementById("canvas2");
    var canvas3: any = document.getElementById("canvas3");

    var source = [70, 90, 50];

    var canvases = [
        canvas1,
        canvas2,
        canvas3
    ];

    var materials = [
        new Material(0.1, 0.9, 5),
        new Material(0.9, 0.1, 200),
        new Material(0.5, 0.5, 10)
    ];

    drawScene(canvases, materials, source);

    var upButton = document.getElementById("up");
    upButton.onclick = () => {
        source = moveSource(source, 0, -step, 0);
        drawScene(canvases, materials, source);
    };

    var downButton = document.getElementById("down");
    downButton.onclick = () => {
        source = moveSource(source, 0, step, 0);
        drawScene(canvases, materials, source);
    };

    var leftButton = document.getElementById("left");
    leftButton.onclick = () => {
        source = moveSource(source, -step, 0, 0);
        drawScene(canvases, materials, source);
    };

    var rightButton = document.getElementById("right");
    rightButton.onclick = () => {
        source = moveSource(source, step, 0, 0);
        drawScene(canvases, materials, source);
    };

    var minusButton = document.getElementById("minus");
    minusButton.onclick = () => {
        source = moveSource(source, 0, 0, -step);
        drawScene(canvases, materials, source);
    };

    var plusButton = document.getElementById("plus");
    plusButton.onclick = () => {
        source = moveSource(source, 0, 0, step);
        drawScene(canvases, materials, source);
    };


};

function drawSphere(canvas: HTMLCanvasElement, material: Material, source: number[]) {
    var center = canvas.width / 2;

    var drawer = new Drawer(canvas);
    drawer.drawCircle(100, "red");
    //var material = new Material(0.5, 0.5, 10);

    for (var i = 0; i < canvas.height; i++) {
        for (var j = 0; j < canvas.width; j++) {

            if (!drawer.isBackground(i, j)) {
                var pointZ = drawer.calcZ(i - center, j - center);
                var point = [i, j, pointZ];

                var L = Vector.makeVector(point, source);
                var N = point;
                //var V = observer;
                var R = Vector.makeVector(source, point);

                var normL = Vector.normalize(L);
                var normN = Vector.normalize(N);
                //var normV = Vector.normalize(V);

                var dotProduct = Vector.dotProduct(normN, normL);
                var cosine = Vector.calcCosine(R, normN);

                var I = Phong.calcIllumination(point, source, dotProduct, cosine, material);
                var newColor = Phong.evalPixel(point, I);
                //console.log(newColor);

                drawer.setPixelColor(i, j, newColor);
            }
        }
    }
}

function moveSource(source: number[], dx: number, dy: number, dz: number): number[] {
    source[0] += dx;
    source[1] += dy;
    source[2] += dz;

    //console.log(source);

    return source;
}

function writeCoorinates(source: number[]) {
    document.getElementById("coordinates").innerText = "Współrzędne źródła światła: "
    + "[" + source[0] + ", "
    + source[1] + ", "
    + source[2] + "]";
}

function drawScene(canvases, materials, source) {
    for (var i = 0; i < canvases.length; i++) {
        drawSphere(canvases[i], materials[i], source);
        writeCoorinates(source);
    }
}

function clearScene(canvases: HTMLCanvasElement[]): void {

    for (var i = 0; i < canvases.length; i++) {
        var ctx = canvases[i].getContext("2d");
        ctx.clearRect(0, 0, canvases[i].width, canvases[i].height);
    }
}
