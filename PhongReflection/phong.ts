﻿module Phong {

    export function calcFatt(point: number[], light: number[]) {
        var x = (point[0] + light[0]) * (point[0] + light[0]);
        var y = (point[1] + light[1]) * (point[1] + light[1]);
        var z = (point[2] + light[2]) * (point[2] + light[2]);

        return 1 / Math.sqrt(x + y + z);
    }

    var IA = 100.0;
    var IP = 60000.0;
    var KA = 0.4;

    export function calcIllumination(v: number[], light: number[], dotProduct: number, cosine: number, material: Material) {
        var A = IA * KA;

        // lambert
        var D = calcFatt(v, light) * IP * material.kd * dotProduct;

        // odbicie kierunkowe
        var S = calcFatt(v, light) * IP * material.ks * Math.pow(cosine, material.n);

        return A + D + S;
    }

    export function roundColor(c: number) {
        if (c < 0) {
            return 0;
        } else if (c > 255) {
            return 255;
        } else {
            return Math.round(c);
        }
    }

    export function evalPixel(pixel: number[], I) {
        var R = roundColor(I + pixel[0]);
        var G = roundColor(I + pixel[1]);
        var B = roundColor(I + pixel[2]);
        var A = pixel[3];

        return [R, G, B, A];
        //return new Pixel(R, G, B, pixel.a);
    }

    export class Material {
        ks: number;
        kd: number;
        n: number;

        constructor(ks, kd, n) {
            this.ks = ks;
            this.kd = kd;
            this.n = n;
        }
    }
} 