﻿module Vector {
    export function makeVector(v1: number[], v2: number[]) {
        return [v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]];
    }

    export function normalize(v: number[]): number[] {
        var length = calcVectorLength(v);

        var x = v[0] / length;
        var y = v[1] / length;
        var z = v[2] / length;
        return [x, y, z];
    }

    function calcVectorLength(v: number[]) {
        return Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    }

    export function dotProduct(v1: number[], v2: number[]) {
        return v1[0] * v2[0]
             + v1[1] * v2[1]
             + v1[2] * v2[2];
    }

    export function calcCosine(v1: number[], v2: number[]) {
        var length = calcVectorLength(v1) * calcVectorLength(v2);
        return dotProduct(v1, v2) / length;
    }
}


