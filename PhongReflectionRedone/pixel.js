﻿var Pixel = (function () {
    function Pixel(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    };

    Pixel.prototype.isBlack = function () {
        return this.r === 0 && this.g === 0 && this.b === 0 && this.a === 255;
    };

    return Pixel;
})();