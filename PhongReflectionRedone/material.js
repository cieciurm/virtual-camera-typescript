﻿var Material = (function () {
    function Material(ks, kd, n) {
        this.ks = ks;
        this.kd = kd;
        this.n = n;
    };

    return Material;
})();
