﻿var Phong = (function () {
    var IA = 100.0;
    var IP = 60000.0;
    var KA = 0.4;

    function Phong(renderer, material) {
        this.renderer = renderer;
        this.material = material;
        this.lightSource = new Point(0, 0, 200);
        this.observer = new Point(0, 0, 500);
    };

    Phong.prototype.init = function () {
        this.renderer.clear();
        this.renderer.init();
        var center = this.renderer.width / 2;

        for (var x = 0; x < this.renderer.width; x++) {
            for (var y = 0; y < this.renderer.height; y++) {
                var pixel = this.renderer.getPixel(x, y);

                if (!pixel.isBlack()) {
                    var point = this.getZ(x - center, y - center);

                    var L = Vector.fromPoints(point, this.lightSource);
                    var N = Vector.fromPoint(point);
                    var V = Vector.fromPoint(this.observer);
                    var R = Vector.fromPoints(this.lightSource, point);

                    L.normalize();
                    N.normalize();
                    V.normalize();

                    var dotProduct = N.dotProduct(L);
                    var cosAlpha = R.getCos(N);

                    var I = this.getIllumination(point, dotProduct, cosAlpha);
                    var newPixel = this.evalPixel(pixel, I);

                    this.renderer.setPixel(x, y, newPixel);
                }
            }
        }

        this.renderer.render();
    };

    Phong.prototype.fatt = function (point) {
        var x = (point.x + this.lightSource.x) * (point.x + this.lightSource.x);
        var y = (point.y + this.lightSource.y) * (point.y + this.lightSource.y);
        var z = (point.z + this.lightSource.z) * (point.z + this.lightSource.z);

        return 1 / Math.sqrt(x + y + z);
    };

    Phong.prototype.evalPixel = function (pixel, I) {
        var R = this.roundColor(I + pixel.r);
        var G = this.roundColor(I + pixel.g);
        var B = this.roundColor(I + pixel.b);

        return new Pixel(R, G, B, pixel.a);
    }

    Phong.prototype.roundColor = function (c) {
        if (c < 0) {
            return 0;
        } else if (c > 255) {
            return 255;
        } else {
            return Math.round(c);
        }
    };

    Phong.prototype.getIllumination = function (point, dotProduct, cosAlpha) {
        // Å›wiatÅ‚o otoczenia
        var A = IA * KA;

        // odbicie rozproszone (lambertowskie)
        var D = this.fatt(point) * IP * this.material.kd * dotProduct;

        // odbicie kierunkowe
        var S = this.fatt(point) * IP * this.material.ks * Math.pow(cosAlpha, this.material.n);

        return A + D + S;
    };

    Phong.prototype.getZ = function (x, y) {
        var radius = this.renderer.radius;
        var z = Math.sqrt(radius * radius - x * x - y * y);
        return new Point(x, y, Math.round(z));
    };

    Phong.prototype.move = function (v) {
        this.lightSource.x += v.x;
        this.lightSource.y += v.y;
        this.lightSource.z += v.z;
    };

    return Phong;
})();