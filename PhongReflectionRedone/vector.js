﻿var Vector = (function () {
    function Vector(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    };

    Vector.prototype.add = function (v) {
        this.x = this.x + v.x;
        this.y = this.y + v.y;
        this.z = this.z + v.z;
    };

    Vector.prototype.subtract = function (v) {
        this.x = this.x - v.x;
        this.y = this.y - v.y;
        this.z = this.z - v.z;
    };

    Vector.prototype.normalize = function () {
        var length = this.length();
        this.x /= length;
        this.y /= length;
        this.z /= length;
    };

    Vector.prototype.length = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    };

    Vector.prototype.dotProduct = function (that) {
        return this.x * that.x + this.y * that.y + this.z * that.z;
    };

    // A o B = |A| * |B| * cos(a)
    // cos(a) = (A o B) / (|A| * |B|)
    Vector.prototype.getCos = function (v) {
        var length = this.length() * v.length();
        var cos = this.dotProduct(v) / length;
        return cos;
    };

    Vector.fromPoints = function (a, b) {
        return new Vector(b.x - a.x, b.y - a.y, b.z - a.z);
    };

    Vector.fromPoint = function (v) {
        return new Vector(v.x, v.y, v.z);
    };

    Math.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };

    return Vector;
})();