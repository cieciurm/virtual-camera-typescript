﻿window.onload = function () {
    var renderers = [
        new Renderer("material1"),
        new Renderer("material2"),
        new Renderer("material3")
    ];

    var materials = [
        new Material(0.15, 0.75, 5),
        new Material(0.75, 0.25, 100),
        new Material(0.5, 0.5, 10)
    ];

    var phongs = [];

    for (var i = 0; i < renderers.length; i++) {
        var phong = new Phong(renderers[i], materials[i]);
        phong.init();
        phongs.push(phong);
    }

    var direction;

    var upButton = document.getElementById("up");
    upButton.onclick = function () {
        direction = new Vector(0, -10, 0);
        redraw(phongs, direction);
    };

    var downButton = document.getElementById("down");
    downButton.onclick = function () {
        direction = new Vector(0, 10, 0);
        redraw(phongs, direction);
    };

    var leftButton = document.getElementById("left");
    leftButton.onclick = function () {
        direction = new Vector(-10, 0, 0);
        redraw(phongs, direction);
    };

    var rightButton = document.getElementById("right");
    rightButton.onclick = function () {
        direction = new Vector(10, 0, 0);
        redraw(phongs, direction);
    };

    var minusButton = document.getElementById("minus");
    minusButton.onclick = function () {
        direction = new Vector(0, 0, -10);
        redraw(phongs, direction);
    };

    var plusButton = document.getElementById("plus");
    plusButton.onclick = function () {
        direction = new Vector(0, 0, 10);
        redraw(phongs, direction);
    };
};

function redraw(phongs, direction) {
    for (var i = 0; i < phongs.length; i++) {
        phongs[i].move(direction);
        phongs[i].init();
    }
}
//# sourceMappingURL=app.js.map
