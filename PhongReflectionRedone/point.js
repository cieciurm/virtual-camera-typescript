﻿var Point = (function () {
    function Point(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    };

    Point.prototype.add = function (point) {
        this.x = this.x + point.x;
        this.y = this.y + point.y;
        this.z = this.z + point.z;
    };

    Point.prototype.subtract = function (point) {
        this.x = this.x - point.x;
        this.y = this.y - point.y;
        this.z = this.z - point.z;
    };

    Point.prototype.toRadians = function () {
        return new Point(Math.radians(-this.x), Math.radians(-this.y), Math.radians(-this.z));
    };

    Math.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };

    return Point;
})();