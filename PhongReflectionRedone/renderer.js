﻿var Renderer = (function () {
    var LINE_WIDTH = 1;

    function Renderer(canvasId) {
        this.canvas = document.getElementById(canvasId)
        this.context = this.canvas.getContext('2d');
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.centerX = this.width / 2;
        this.centerY = this.height / 2;
        this.radius = 100;
        this.imageData = this.context.createImageData(this.width, this.height);
    };

    Renderer.prototype.clear = function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };

    Renderer.prototype.render = function () {
        this.context.putImageData(this.imageData, 0, 0);
    };

    Renderer.prototype.init = function () {
        var background = new Pixel(0, 0, 0, 255);
        this.drawRectangle(0, 0, this.width, this.height, background);

        var foreground = new Pixel(60, 60, 60, 255);
        this.drawCircle(this.centerX, this.centerY, this.radius, foreground);
        this.render();
    };

    Renderer.prototype.getPixel = function (x, y) {
        var index = (x + y * this.imageData.width) * 4;
        return new Pixel(this.imageData.data[index + 0],
                         this.imageData.data[index + 1],
                         this.imageData.data[index + 2],
                         this.imageData.data[index + 3])
    }

    Renderer.prototype.setPixel = function (x, y, pixel) {
        var index = (x + y * this.imageData.width) * 4;
        this.imageData.data[index + 0] = pixel.r;
        this.imageData.data[index + 1] = pixel.g;
        this.imageData.data[index + 2] = pixel.b;
        this.imageData.data[index + 3] = pixel.a;
    }

    Renderer.prototype.drawRectangle = function (x0, y0, width, height, pixel) {
        for (var i = y0; i < y0 + height; i++) {
            this.drawHorizontalLine(x0, x0 + width, i, pixel);
        }
    };

    Renderer.prototype.drawCircle = function (x0, y0, radius, pixel) {
        var x = radius - 1;
        var y = 0;
        var radiusError = 1 - x;

        while (x >= y) {
            this.drawHorizontalLine(x0 - x, x0 + x, y + y0, pixel);
            this.drawHorizontalLine(x0 - x, x0 + x, -y + y0, pixel);
            this.drawHorizontalLine(x0 - y, x0 + y, x + y0, pixel);
            this.drawHorizontalLine(x0 - y, x0 + y, -x + y0, pixel);

            y++;

            if (radiusError < 0) {
                radiusError += 2 * y + 1;
            }
            else {
                x--;
                radiusError += 2 * (y - x + 1);
            }
        }
    };

    Renderer.prototype.drawHorizontalLine = function (x0, x1, y, pixel) {
        for (var i = x0; i < x1; i++) {
            this.setPixel(i, y, pixel);
        }
    };

    return Renderer;
})();